import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/vuetify',
    name: 'Vuetify',
    component: () => import('../views/Vuetify.vue')
  },
  {
    path: '/le-viajes',
    name: 'Le-viajes',
    component: () => import('../views/Le-viajes.vue')
  },
  {
    path: '/memes',
    name: 'Memes',
    component: () => import('../views/Memes.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
