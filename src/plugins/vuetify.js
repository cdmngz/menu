import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
          customProperties: true,
        },
      themes: {
        light: {
          primary: '#008080',
          secondary: '#254184',
          tertiary: '#C0C0C0',
          accent: '#82B1FF',
          error: '#FF5252',
          info: '#2196F3',
          success: '#EBF2FF',
          warning: '#ee44aa',         
        },
        dark: {
          primary: '#cecece',
          secondary: '#000',
          tertiary: '#000',
          accent: '#000',
          error: '#ff5656',
          info: '#000',
          success: '#000',
          warning: '#FF0000',         
        },
      },
    },
});
